import React, {useState, useEffect} from 'react';
import './App.css';
import styled from '@emotion/styled'
import Frase from './componentes/Frase';
const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
`
const  Boton = styled.button`
  background: -webKit-linear-gradient(top left, #007d35 0%, #007d35 40%, #0f574e 100%);
  background-size: 300px;
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 3rem;
  font-size:2rem;
  border: 2px solid black;
`
function App() {
  const [frase, guaradarFrase]= useState({});
  const consultarApi = async ()=>{
    const api = await fetch('https://breaking-bad-quotes.herokuapp.com/v1/quotes');
    const frase = await api.json();
    guaradarFrase(frase[0]);

  }
  //cargar una frase
  useEffect(()=>{
     consultarApi();
  }, [])
  return (
    <Contenedor>
      <Boton
        onClick={consultarApi}
      >
        Obtener frase
      </Boton>
      <Frase
        frase={frase}
      />
    </Contenedor>
  );
}

export default App;
